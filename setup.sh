#!/bin/bash

## Set Global Git Configurations
git config --global user.email "jet.48@protonmail.com"
git config --global user.name  "James Tyler"

## Install Packages
paru --noconfirm -S                            \
    brave-bin                                  \
    ccls                                       \
    kitty                                      \
    kwin-bismuth                               \
    lazygit                                    \
    librewolf-bin                              \
    neofetch                                   \
    neovim-nightly-bin                         \
    otf-font-awesome                           \
    plasma5-applets-virtual-desktop-bar-git    \
    ripgrep                                    \
    tree                                       \
    ttf-jetbrains-mono                         \
    vim                                        \
    xclip                                      \
    zsh

## Change Shell to ZSH
chsh -s $(which zsh)

## Clone the Dotfiles Repo
if [ ! -e "${HOME}/projects/dotfiles" ]; then
    mkdir -p ~/projects
    pushd ~/projects
    git clone git@gitlab.com:jtyler76/dotfiles.git
    popd
fi

## Install the Neovim Configuration
rm -rf ${HOME}/.config/nvim
ln -sf ${HOME}/projects/dotfiles/config/nvim ${HOME}/.config/nvim

## Install the Kitty Configuration
rm -rf ${HOME}/.config/kitty
ln -sf ${HOME}/projects/dotfiles/config/kitty ${HOME}/.config/kitty

## Install the ZSH Configuration
rm -rf ${HOME}/.oh-my-zsh
sh -c "$(curl -fsSL https://raw.githubusercontent.com/ohmyzsh/ohmyzsh/master/tools/install.sh)" \
      "" --unattended
rm -rf ${HOME}/.zshrc ${HOME}/.zshrc.pre-oh-my-zsh
ln -sf ${HOME}/projects/dotfiles/zshrc ${HOME}/.zshrc
ln -sf ${HOME}/projects/dotfiles/oh-my-zsh/themes/jetyler.zsh-theme \
       ${HOME}/.oh-my-zsh/themes/jetyler.zsh-theme

## Install the Librewolf Configuration
mkdir -p ${HOME}/.librewolf
ln -sf ${HOME}/projects/dotfiles/librewolf/librewolf.overrides.cfg \
       ${HOME}/.librewolf/librewolf.overrides.cfg
